package sairepa.model.fields;

import sairepa.model.Act;

public interface Test
{
  public boolean test(Act a);
}
